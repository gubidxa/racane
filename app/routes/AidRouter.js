const express = require('express');
const AidController = require('../controllers/AidController');

const AidRouter = express.Router();

AidRouter.get('/:country/:year', AidController.getAidBudgetByCountry);

module.exports = AidRouter;
