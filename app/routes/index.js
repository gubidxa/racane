const express = require('express');
const AidRouter = require('./AidRouter');

const Router = express.Router();

Router.use('/aid', AidRouter);

module.exports = Router;
