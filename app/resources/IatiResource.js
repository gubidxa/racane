const IatiResource = module.exports;

const bent = require('bent');

const getFromIati = bent('https://iatidatastore.iatistandard.org', 'json');

IatiResource.getAidBudgetByCountry = async (country) => {
  const qs = [
    `recipient_country=${country}`,
    'aggregations=sector,value',
    'group_by=budget_period_start_year,sector',
    'format=json',
  ];
  const uri = `/api/budgets/aggregations/?${qs.join('&')}`;
  const result = await getFromIati(uri);

  return result;
};
