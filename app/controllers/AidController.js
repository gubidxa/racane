const AidController = module.exports;

const AidService = require('../services/AidService');

AidController.getAidBudgetByCountry = async (req, res) => {
  /*  #swagger.tags = ['User']
        #swagger.description = 'Endpoint to get aid budget by country' */

  /*  #swagger.parameters['country'] = {
            in: 'path',
            description: 'Two-letter country codes defined in ISO 3166-1 alpha-2',
            required: true,
            type: 'string'
    } */
  /*  #swagger.parameters['year'] = {
            in: 'path',
            description: 'Year in which the monetary aid is made',
            required: true,
            type: 'integer'
    } */
  const { country, year } = req.params;

  return res.json(await AidService.getAidBudgetByCountry(country, year));
};
