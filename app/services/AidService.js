const AidService = module.exports;

const IatiResource = require('../resources/IatiResource');

AidService.getAidBudgetByCountry = async (country, year) => {
  const startYear = year - 5;
  const endYear = year;
  const response = await IatiResource.getAidBudgetByCountry(country);

  return response.results
    .map(({
      budget_period_start_year: budgetYear,
      sector: { name: sectorName },
      value,
    }) => ({ budgetYear, sectorName, value }))
    .filter(({ budgetYear }) => (budgetYear >= startYear && budgetYear <= endYear))
    .reduce((accumulated, item) => {
      const grouped = accumulated;
      const { budgetYear, sectorName, value } = item;
      grouped[budgetYear] = {
        ...(grouped[budgetYear] || {}),
        [sectorName]: value,
      };

      return grouped;
    }, {});
};
