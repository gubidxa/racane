const swaggerAutogen = require('swagger-autogen')();

const doc = {
  info: {
    title: 'Aid API',
    description: 'Application to show monetary aid by country',
  },
  host: 'localhost:3000',
  basePath: '/api',
  schemes: ['http'],
};

const outputFile = './doc/swagger_output.json';
const endpointsFiles = ['./app/routes/index.js'];

swaggerAutogen(outputFile, endpointsFiles, doc);
