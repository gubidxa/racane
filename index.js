const express = require('express');
const { middleware: cache } = require('apicache');
const swaggerUi = require('swagger-ui-express');

const routes = require('./app/routes');
const swaggerFile = require('./doc/swagger_output.json');

const app = express();
const port = process.env.APP_PORT || 3000;

app.use(express.json());
app.use('/api', cache('5 minutes'), routes);
app.get('/', (_, res) => res.send('Hello World!'));
app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile));

app.listen(port, () => {
  console.log(`Racane app listening at ${port}`);
});

module.exports = app;
