# RACANÉ

Racané is a application to show monetary aid by country.

Install the dependencies:

```bash
$ npm install
```

Use the command below to generate the documentation:

```bash
$ npm run start-gendoc
```

Use the command below to start the project:

```bash
$ npm start
```

Access the documentation at:

  [http://localhost:3000/doc](http://localhost:3000/doc)








<sub>Fun fact: _racané_ means _help_ in _Zapoteco Istmeño_ language</sub>
