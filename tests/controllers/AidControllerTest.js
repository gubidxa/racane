const chai = require('chai');
const chaiHttp = require('chai-http');
const sandbox = require('sinon').createSandbox();
const app = require('../../index');
const IatiResourceData = require('../data/IatiResourceData');
const IatiResource = require('../../app/resources/IatiResource');

chai.use(chaiHttp);
const { assert } = chai;

const country = 'MX';
const year = 2015;

describe('AidController', () => {
  describe(`GET /api/aid/${country}/${year}`, () => {
    beforeEach(() => {
      sandbox.spy();
    });
    afterEach(() => sandbox.restore());

    it('Should be return a empty object', async () => {
      sandbox.stub(IatiResource, 'getAidBudgetByCountry').resolves(IatiResourceData.emptyObject);

      const result = await chai.request(app).get(`/api/aid/${country}/${year - 1}`);

      assert.deepEqual(result.status, 200);
      assert.deepEqual(result.body, {});
    });

    it('Should be return a nice object', async () => {
      sandbox.stub(IatiResource, 'getAidBudgetByCountry').resolves(IatiResourceData.niceObject);

      const result = await chai.request(app).get(`/api/aid/${country}/${year}`);

      assert.deepEqual(result.status, 200);
      assert.deepEqual(result.body, {
        2013: {
          'Vocabulary 99 or 98': 5120000,
        },
        2014: {
          'Vocabulary 99 or 98': 25000,
        },
        2015: {
          Livestock: 1220000.04,
        },
      });
    });

    it('Iati Resource should be called once', async () => {
      sandbox.stub(IatiResource, 'getAidBudgetByCountry').resolves(IatiResourceData.niceObject);

      await chai.request(app).get(`/api/aid/${country}/${year + 1}`);
      await chai.request(app).get(`/api/aid/${country}/${year + 1}`);
      await chai.request(app).get(`/api/aid/${country}/${year + 1}`);

      assert.deepEqual(IatiResource.getAidBudgetByCountry.callCount, 1);
    });
  });
});
