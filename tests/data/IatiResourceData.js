module.exports = {
  emptyObject: {
    count: 0,
    results: [],
  },
  niceObject: {
    count: 3,
    results: [
      {
        budget_period_start_year: 2015,
        sector: {
          url: 'http://iatidatastore.iatistandard.org/api/sectors/31163/?format=json',
          code: '31163',
          name: 'Livestock',
        },
        value: 1220000.04,
      },
      {
        budget_period_start_year: 2014,
        sector: {
          url: 'http://iatidatastore.iatistandard.org/api/sectors/cp/?format=json',
          code: 'cp',
          name: 'Vocabulary 99 or 98',
        },
        value: 25000.0,
      },
      {
        budget_period_start_year: 2013,
        sector: {
          url: 'http://iatidatastore.iatistandard.org/api/sectors/52010/?format=json',
          code: '52010',
          name: 'Vocabulary 99 or 98',
        },
        value: 5120000.0,
      },
    ],
  },
};
