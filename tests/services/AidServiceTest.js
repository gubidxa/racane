const { assert } = require('chai');
const sandbox = require('sinon').createSandbox();
const IatiResourceData = require('../data/IatiResourceData');
const IatiResource = require('../../app/resources/IatiResource');
const AidService = require('../../app/services/AidService');

describe('AidService', () => {
  describe('getAidBudgetByCountry() function', () => {
    afterEach(() => sandbox.restore());

    it('Should be return a empty object', async () => {
      sandbox.stub(IatiResource, 'getAidBudgetByCountry').resolves(IatiResourceData.emptyObject);

      const result = await AidService.getAidBudgetByCountry('MX', 2021);
      assert.deepEqual(result, {});
    });

    it('Should be return a well object', async () => {
      sandbox.stub(IatiResource, 'getAidBudgetByCountry').resolves(IatiResourceData.niceObject);

      const result = await AidService.getAidBudgetByCountry('MX', 2015);
      assert.deepEqual(result, {
        2013: {
          'Vocabulary 99 or 98': 5120000,
        },
        2014: {
          'Vocabulary 99 or 98': 25000,
        },
        2015: {
          Livestock: 1220000.04,
        },
      });
    });
  });
});
